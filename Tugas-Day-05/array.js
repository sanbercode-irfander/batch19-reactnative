//Soal 01 (Range)
function range(startNum, finishNum) {
    var myarray = [];

    if((startNum == null) || (finishNum == null)) {
        //Just return -1 if any null parameter found
        return -1;
    } else if(startNum < finishNum) {
        //Incremental Array Data
        for(x=startNum;x<=finishNum;x++) {
            myarray.push(x);
        }
        return myarray;
    } else if(startNum > finishNum) {
        //Decremental Array Data
        for(x=startNum;x>=finishNum;x--) {
            myarray.push(x);
        }
        return myarray;
    } else {
        //If startNum == finishNum, nothing is returned
        return null;
    }

}

console.log("Output Soal 1:") 
console.log(range(1, 10)) 
console.log(range(1)) 
console.log(range(11,18)) 
console.log(range(54, 50)) 
console.log(range()) 


//Soal 02 (Range with Step)
function rangeWithStep(startNum, finishNum, step) {
    var myarray = [];

    if((startNum == null) || (finishNum == null)) {
        //Just return -1 if any null parameter found
        return -1;
    } else if(startNum < finishNum) {
        //Incremental Array Data
        while(startNum <= finishNum) {
            myarray.push(startNum);
            startNum = startNum + step;
        }
        return myarray;
    } else if(startNum > finishNum) {
        //Decremental Array Data
        while(startNum >= finishNum) {
            myarray.push(startNum);
            startNum = startNum - step;
        }
        return myarray;
    } else {
        //If startNum == finishNum, nothing is returned
        return null;
    }

}

console.log("Output Soal 2:") 
console.log(rangeWithStep(1, 10, 2))
console.log(rangeWithStep(11, 23, 3))
console.log(rangeWithStep(5, 2, 1))
console.log(rangeWithStep(29, 2, 4))

//Soal 3 (Sum of Range):
function sum(param1,param2,step) {
    var myarray = [];
    var total = 0;

    // Cek Step
    if(step == null) { 
        step = 1; 
    }

    if(param1 < param2) {
        //Incremental Array Data
        while(param1 <= param2) {
            myarray.push(param1);
            param1 = param1 + step;
        }
    } else if(param1 > param2) {
        //Incremental Array Data
        while(param1 >= param2) {
            myarray.push(param1);
            param1 = param1 - step;
        }
    } else if(param2 == null) {
        if(param1 == null) {
            return 0;
        } else {
            return param1;
        }
    }


    //Sum Array Member
    for(i = 0; i <myarray.length; i++){
        total += myarray[i];
    }

    return total;
}

console.log("Output Soal 3:") 
console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

//Soal 4 (Array Multidimensi):
function dataHandling(arraySet) {
    var result = "";

    for(i = 0; i <arraySet.length; i++){
       result += "\n\nNomor ID: " + arraySet[i][0] +
                "\nNama Lengkap: " + arraySet[i][1] +
                "\nTTL: " + arraySet[i][2] + " " + arraySet[i][3] +
                "\nHobi: " + arraySet[i][4];
    }

    result += "\n\n";
    return result;

}

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
];

console.log("Output Soal 4:") 
console.log(dataHandling(input));

//Soal 5 (Balik Kata)
function balikKata(param1) {
    return param1.split("").reverse().join("");
}

console.log("Output Soal 5:")
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 

//Soal 6 (Metod Array)
function dataHandling2(arraySet) {

    //Tampilkan Array
    arraySet.splice(1, 1, "Roman Alamsyah Elsharawy")
    arraySet.splice(2, 1, "Provinsi Bandar Lampung")
    arraySet.splice(4, 0, "Pria")
    arraySet.splice(5, 1, "SMA Internasional Metro")
    console.log(arraySet);

    //Tampilkan bulan
    var splitArrayTgl = arraySet[3].split("/");
    var namabulan = "";
    switch (splitArrayTgl[1]) {
        case '01' : namabulan = 'Januari'; break;
        case '02' : namabulan = 'Februar'; break;
        case '03' : namabulan = 'Maret'; break;
        case '04' : namabulan = 'April'; break;
        case '05' : namabulan = 'Mei'; break;
        case '06' : namabulan = 'Juni'; break;
        case '07' : namabulan = 'Juli'; break;
        case '08' : namabulan = 'Agustus'; break;
        case '09' : namabulan = 'September'; break;
        case '10' : namabulan = 'Oktober'; break;
        case '11' : namabulan = 'November'; break;
        case '12' : namabulan = 'Desember'; break;
        default: namabulan = ''; break;
    }
    console.log(namabulan);

    //Tampilkan split tanggal
    console.log(splitArrayTgl.sort(function (value1, value2) { return value2 - value1 } ));

    //Join Tanggal
    console.log(arraySet[3].split("/").join("-"));

    //Slice Nama
    console.log(arraySet[1].slice(0,14));
}

console.log("\nOutput Soal 6:")
var input = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);