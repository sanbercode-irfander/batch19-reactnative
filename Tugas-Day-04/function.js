/* No 1 */
function teriak(param="Halo Sanbers!") {
    return param;
}
 
console.log(teriak())

/* No 2 */
var num1 = 12
var num2 = 4
 
function kalikan(param1,param2) {
    return param1*param2
}
var hasilKali = kalikan(num1, num2)
console.log(hasilKali) // 48

/* No 3 */
var name = "Agus"
var age = 30
var address = "Jln. Malioboro, Yogyakarta"
var hobby = "Gaming"
 
function introduce(parname,parage,paraddress,parhobby) {
    return "Nama saya " + parname + " , umur saya " + parage + " ,alamat saya di " + paraddress + " , dan saya punya hobby " + parhobby + "!"
}

var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan) // Menampilkan "Nama saya Agus, umur saya 30 tahun, alamat saya di Jln. Malioboro, Yogyakarta, dan saya punya hobby yaitu Gaming!" 